# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


inOut = ->
  $("#average").mouseover ->
    $("#description").fadeIn('slow')
    return
  $("#average").mouseout ->
    $("#description").fadeOut('slow')
    return
  return

$(document).ready(inOut)
$(document).on('page:load', inOut)
