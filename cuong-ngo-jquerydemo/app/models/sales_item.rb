class SalesItem < ActiveRecord::Base
  belongs_to :category

  def totalItem
    SalesItem.count
  end

  def avgPrice
    total = 0
    count = SalesItem.count
    items = SalesItem.all
    items.each do |item|
      total = total + item.price
    end
    avg = total / count
  end

end
